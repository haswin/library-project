export default function() {
  this.namespace = '/api';

  this.get('/books', function() {
    return {
      data: [{
        type: 'books',
        id: 'the-alchemist',
        attributes: {
          title: 'The Alchemist',
          author: 'Paulo Coelho',
          availability:4,
          genre: 'Fiction'
        }
      },{
        type: 'books',
        id: 'three-twins',
        attributes: {
          title: 'Three twins',
          author: 'Mark twain',
          availability:4,
          genre: 'horror/comedy'
        }
      },{
        type: 'books',
        id: 'urban-legend',
        attributes: {
          title: 'Urban Legend',
          author: 'Mike lawyer',
          availability:8,
          genre: 'Rom/com'
        }
      },{
        type: 'books',
        id: 'the-sot',
        attributes: {
          title: 'Subtle Art',
          author: 'Mike Manson',
          availability:2,
          genre: 'philosophy'
        }
      }, {
        type: 'books',
        id: 'mentalist',
        attributes: {
          title: 'The Mentalist',
          author: 'Patrick jane',
          availability:4,
          genre: 'crime'
        }
      }]
    };
  });
  
  this.post('/books');
  
  this.get('/books/:id', (schema, request) => {
    let id = request.params.id;
  
    return schema.books.find(id); // users in the second case
  });

  //this.get('/books/:book_id');
  //this.patch('/books/:book_id'); 
  
  this.patch('/books/:id', function(schema, request) {
    let id = request.params.id;
    let attrs = this.normalizedRequestAttrs();
  
    return schema.books.find(id).update(attrs);
  });



  this.get('/users', function() {
    return {
      data: [{
        type: 'users',
        id: 'john',
        attributes: {
          name: 'John',
          email: 'john@gmail.com',
          phone:7777788788,
          city: 'Coimbatore'
        }
      },{
        type: 'users',
        id: 'wick',
        attributes: {
          name: 'Wick',
          email: 'wick@gmail.com',
          phone:7708459625,
          city: 'Salem'
        }
      },{
        type: 'users',
        id: 'hamilton',
        attributes: {
          name: 'Hamilton',
          email: 'hamilton@gmail.com',
          phone:8675960856,
          city: 'Seattle'
        }
      },{
        type: 'users',
        id: 'mark',
        attributes: {
          name: 'Mark',
          email: 'mark@gmail.com',
          phone:7598258471,
          city: 'trichy'
        }
      }, {
        type: 'users',
        id: 'raj',
        attributes: {
          name: 'Raj',
          email: 'raj@gmail.com',
          phone:5463546521,
          city: 'Portland'
        }
      }]
    };
  });
  
  this.post('/users');



}


