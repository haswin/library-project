import DS from 'ember-data';

export default DS.Model.extend({
    title: DS.attr(),
    author: DS.attr(),
    availability: DS.attr(),
    genre: DS.attr()
});
