import Route from '@ember/routing/route';

export default Route.extend({
    actions: {

        saveUser() {
          
        const name=this.get('controller').get('name');
        const email=this.get('controller').get('email');
        const city=this.get('controller').get('city');
        const phone=this.get('controller').get('phone');
        const newUser=this.store.createRecord('user',{name,email,city,phone});
        newUser.save().then(() => this.transitionTo('users'));
        }
      }
});
