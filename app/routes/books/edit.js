import Route from '@ember/routing/route';

export default Route.extend({
  model(params) {
    return this.get('store').findRecord('book', params.book_id);
  },
  actions: {
    saveChanges(book) {
      book.save();
      this.transitionTo('books');
    }
   }
});
