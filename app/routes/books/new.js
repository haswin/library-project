import Route from '@ember/routing/route';

export default Route.extend({
    
  actions: {

    saveBook() {
      const title=this.get('controller').get('title');
      const author=this.get('controller').get('author');
      const genre=this.get('controller').get('genre');
      const availability=this.get('controller').get('availability');
      const newBook=this.store.createRecord('book',{title,author,genre,availability});
      newBook.save().then(() => this.transitionTo('books'));
    }
  }
});

